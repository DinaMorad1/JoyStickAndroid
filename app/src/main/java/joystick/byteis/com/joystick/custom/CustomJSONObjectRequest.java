package joystick.byteis.com.joystick.custom;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dina on 10/11/2015.
 */
public class CustomJSONObjectRequest extends JsonObjectRequest {
    private Context context;
    public CustomJSONObjectRequest(int method, String url, JSONObject jsonRequest,
                                   Response.Listener<JSONObject> listener,
                                   Response.ErrorListener errorListener,Context context) {
        super(method, url, jsonRequest, listener, errorListener);
        this.context = context;
    }

    @Override
    public Map<String, String> getHeaders() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json; charset=utf-8;");
        return headers;
    }

//    Map<String, String> createBasicAuthHeader() {
//        Map<String, String> headerMap = new HashMap<String, String>();
//
//        String credentials = username + ":" + password;
//        String encodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
////        headerMap.put("Authorization", "Basic " + encodedCredentials);
//
//        headerMap.put("Content-Type", "application/json; charset=utf-8;");// application/x-www-form-urlencoded;");
//
//
//        return headerMap;
//    }

//    @Override
//    public RetryPolicy getRetryPolicy() {
//        // here you can write a custom retry policy
//        return new DefaultRetryPolicy(15000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);//super.getRetryPolicy();
//    }
}
