package joystick.byteis.com.joystick.client;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import joystick.byteis.com.joystick.R;
import joystick.byteis.com.joystick.custom.CustomJSONObjectRequest;

/**
 * Created by Dina on 15/11/2015.
 */
public class VolleyHelper {

    protected Context context;
    protected RequestQueue mRequestQueue;

    protected VolleyHelper(Context context) {
        this.context = context;
        mRequestQueue = Volley.newRequestQueue(context);
    }

    protected void scheduleJsonObjectRequest(int request, String requestUrl, JSONObject jsonObject, final ClientCallback clientCallback, final Class<?> classType) {
        CustomJSONObjectRequest jsonObjReq = new CustomJSONObjectRequest(request,
                requestUrl, jsonObject,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Object object = null;
                        String responseMessage = "";
                        if (response.has(ApiClient.RESPONSE_MESSAGE)) {
                            try {
                                responseMessage = response.getString(ApiClient.RESPONSE_MESSAGE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        //parse returned object
                        if (classType != null) {
                            Gson gson = new Gson();
                            object = gson.fromJson(response.toString(), classType);
                        }
                        clientCallback.onSuccess(object, responseMessage);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "";

                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    errorMessage = context.getResources().getString(R.string.no_network);
                } else {
                    String json = null;

                    NetworkResponse response = error.networkResponse;
                    if (response != null && response.data != null) {
                        json = new String(response.data);
                        json = trimMessage(json, "message");
                        if (json != null)
                            errorMessage = json;
                    }
                }
                clientCallback.onFailure(errorMessage);
            }
        }, context);

//        jsonObjReq.getHeaders().a
        jsonObjReq.setRetryPolicy(new

                        DefaultRetryPolicy(75000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

        );
        mRequestQueue.add(jsonObjReq);
    }

    public String trimMessage(String json, String key) {
        String trimmedString = null;

        try {
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

        return trimmedString;
    }

}
