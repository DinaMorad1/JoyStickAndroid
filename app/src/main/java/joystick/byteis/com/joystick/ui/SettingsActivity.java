package joystick.byteis.com.joystick.ui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import joystick.byteis.com.joystick.R;
import joystick.byteis.com.joystick.data.SharedPref;
import joystick.byteis.com.joystick.databinding.ActivitySettingsBinding;
import joystick.byteis.com.joystick.model.User;

/**
 * Created by Dina on 16/11/2015.
 */
public class SettingsActivity extends BaseActivity {

    private ActivitySettingsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(SettingsActivity.this, R.layout.activity_settings);
    }

    public void onEditProfileClicked(View view) {
        Intent editProfileIntent = new Intent(this, EditProfileActivity.class);
        startActivityWithTransition(this, editProfileIntent);
    }

    public void onChangePasswordClicked(View view) {
        Intent changePasswordIntent = new Intent(SettingsActivity.this, ChangePasswordActivity.class);
        startActivityWithTransition(SettingsActivity.this, changePasswordIntent);
    }

    public void onChangeEmailClicked(View view) {
        Intent changeEmailIntent = new Intent(SettingsActivity.this, ChangeEmailActivity.class);
        startActivityWithTransition(SettingsActivity.this, changeEmailIntent);
    }

    public void onSignoutButtonClicked(View view) {
        if (isLoggedIn())
            LoginManager.getInstance().logOut();

        SharedPref.deleteShared(this);
        User.removeLoginUser();
        Intent loginIntent = new Intent(this, LoginActivity.class);
        startActivityWithTransition(this, loginIntent);
        finishActivity(this);
    }

    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }
}
