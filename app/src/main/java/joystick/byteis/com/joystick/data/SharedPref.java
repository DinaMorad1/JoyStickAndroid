package joystick.byteis.com.joystick.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Dina on 11/11/2015.
 */
public class SharedPref {

    public static final String USER_TOKEN = "USER_TOKEN";
    public static final String FACEBOOK_TOKEN = "FACEBOOK_TOKEN";
    public static final String Twitter_Token = "TWITTER_TOKEN";


    /* Shared preference keys for login with twitter */
/*    public static final String PREF_NAME = "sample_twitter_pref";
    public static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    public static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    public static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_loggedin";
    public static final String PREF_USER_NAME = "twitter_user_name";*/
    public static final String ENCRYPRION_KEY = "ENCRYPTION_KEY";
    public static final String PASSWORD_KEY = "PASSWORD_KEY";


    public static int LoadInt(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        int data;

        if(prefs.contains(key)){
            data = prefs.getInt(key,0);
        }else{
            data = 0;
        }
        return data;
    }

    public static void SaveInt(Context context, String key, int value) {
        SharedPreferences prefs = PreferenceManager .getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefEditor = prefs.edit();
        prefEditor.putInt(key, value);
        prefEditor.commit();
    }

    public static String LoadString(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String data;

        if(prefs.contains(key)){
            data = prefs.getString(key,"");
        }else{
            data = "";
        }
        return data;
    }

    public static void SaveString(Context context, String key, String value) {
        SharedPreferences prefs = PreferenceManager .getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefEditor = prefs.edit();
        prefEditor.putString(key, value);
        prefEditor.commit();
    }

    public static void SaveBoolean(Context context ,String key , boolean value){
        SharedPreferences prefs = PreferenceManager .getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefEditor = prefs.edit();
        prefEditor.putBoolean(key, value);
        prefEditor.commit();
    }

    public static void SaveLong(Context context ,String key , long value){
        SharedPreferences prefs = PreferenceManager .getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefEditor = prefs.edit();
        prefEditor.putLong(key, value);
        prefEditor.commit();
    }

    public static long LoadLong(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        long data;

        if(prefs.contains(key)){
            data = prefs.getLong(key,0);
        }else{
            data = 0;
        }
        return data;
    }

    public static boolean LoadBoolean(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean data;

        if(prefs.contains(key)){
            data = prefs.getBoolean(key,false);
        }else{
            data = false;
        }
        return data;
    }
    public static void deleteShared(Context context){
        SharedPreferences prefs = PreferenceManager .getDefaultSharedPreferences(context);
        SharedPreferences.Editor prefEditor = prefs.edit();
        prefEditor.clear();
        prefEditor.commit();
    }

    public static boolean checkKey(Context context,String key){
        return PreferenceManager.getDefaultSharedPreferences(context).contains(key);
    }
}
