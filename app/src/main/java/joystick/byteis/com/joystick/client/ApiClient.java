package joystick.byteis.com.joystick.client;

import android.graphics.Bitmap;

/**
 * Created by Dina on 10/11/2015.
 */
public interface ApiClient {

    /*request urls*/
    public static final String USER_SIGNIN_URL = "/users/sign_in";
    public static final String FORGOT_PASSWORD_URL = "/users/password/new";
    public static final String USERS_URL = "/users";
    public static final String NEW_PASSWORD_URL = "/users/password?new_password";


    /*common fields*/
    public static final String RESPONSE_MESSAGE = "message";

    /*request fields*/
    /*user*/
    public static final String USER = "user";
    public static final String USER_EMAIL = "email";
    public static final String USER_PASSWORD = "password";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String BIRTH_DATE = "birth_date";
    public static final String TOKEN = "auth_token";
    public static final String SOCIAL_PROVIDER = "provider";
    public static final String SOCIAL_TOKEN = "access_token";
    public static final String SOCIAL_FACEBOOK_PROVIDER = "facebook";
    public static final String SOCIAL_TWITTER_PROVIDER = "twitter";
    public static final String SOCIAL_USER_ID = "uid";
    public static final String USER_CONFIRMED_PASSWORD = "password_confirmation";
    public static final String USER_AVATAR_CONTENT_TYPE = "avatar_content_type";
    public static final String USER_AVATAR_DATA = "avatar_data";

    public void userLogin(String userEmail, String password, ClientCallback clientCallback);

    public void forgotPassword(String userEmail, ClientCallback callback);

    public void userSignUp(String userEmail, String password, String firstName, String lastName, String formattedBirthDate, ClientCallback clientCallback);

    public void changeEmail(String newEmail, ClientCallback callback);

    public void socialLogin(String email, String password, String socialToken, String provider, String userId, ClientCallback clientCallback);

    public void socialSignup(String email, String password, String socialToken, String firstName, String lastName, String provider, String userId, String birthDate, ClientCallback clientCallback);

    public void changePassword(String newPassword, String confirmedNewPassword, ClientCallback clientCallback);

    public void updateAvatar(Bitmap bitmap, ClientCallback callback);

    public void updateUserProfile(Bitmap bitmap, String firstName, String email, ClientCallback callback);
}
