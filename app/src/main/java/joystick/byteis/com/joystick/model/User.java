package joystick.byteis.com.joystick.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.ArrayList;

/**
 * Created by Dina on 10/11/2015.
 */
@Table(name = "User")
public class User extends Model {

    @Column(name = "createdAt")
    private String created_at;

    @Column(name = "updatedAt")
    private String updated_at;

    @Column(name = "locale")
    private String locale;

    @Column(name = "expirationDate")
    private String auth_expiration_date;

    @Column(name = "roles")
    private ArrayList<String> roles;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "firstName")
    private String first_name;

    @Column(name = "lastName")
    private String last_name;

    @Column(name = "gender")
    private String gender;

    @Column(name = "birthDate")
    private String birth_date;

//    @Column(name = "auth_token")
    private String auth_token;

    private Object avatar;

    @Column(name = "avatar")
    private String avatarUrl;

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }


    public Object getAvatar() {
        return avatar;
    }

    public static User getCurrentUser(){
        User returnUser = (User) new Select().from(User.class).execute().get(0);
        return returnUser;
    }

    public static void removeLoginUser(){
        new Delete().from(User.class).execute();
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(String birth_date) {
        this.birth_date = birth_date;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<String> getRoles() {
        return roles;
    }

    public void setRoles(ArrayList<String> roles) {
        this.roles = roles;
    }

    public String getAuth_expiration_date() {
        return auth_expiration_date;
    }

    public void setAuth_expiration_date(String auth_expiration_date) {
        this.auth_expiration_date = auth_expiration_date;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

}
