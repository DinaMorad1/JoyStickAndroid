package joystick.byteis.com.joystick.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.transition.Slide;
import android.transition.Transition;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import joystick.byteis.com.joystick.R;
import joystick.byteis.com.joystick.client.ApiClient;
import joystick.byteis.com.joystick.client.ApiClientImp;
import joystick.byteis.com.joystick.util.UtilityMethods;

/**
 * Created by Dina on 11/11/2015.
 */
public class BaseActivity extends AppCompatActivity {

    protected ProgressDialog progressDialog;
    protected ApiClient apiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiClient = ApiClientImp.getInstance(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.primary_color_dark));
        }

        applyTransition(this);
    }

    protected ProgressDialog showProgressDialog(String dialogMessage, boolean isCancelable, boolean isIndeteminate){
        ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage(dialogMessage);
        pd.setCancelable(isCancelable);
        pd.setIndeterminate(isIndeteminate);
        pd.show();
        return pd;
    }

    protected void setError(TextInputLayout textInputLayout, String error) {
        textInputLayout.setError(error);
        textInputLayout.getEditText().setError(error);
    }

    protected boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected Activity applyTransition(Activity activity){
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            Transition transition = new Slide();
            ((Slide)transition).setSlideEdge(Gravity.RIGHT);
            transition.excludeTarget(android.R.id.statusBarBackground, true);
            transition.excludeTarget(android.R.id.navigationBarBackground, true);
            activity.getWindow().setEnterTransition(transition);
//            activity.getWindow().setExitTransition(transition);
        }
        return activity;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void startActivityWithTransition(Activity activity, Intent intent){
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity,null);
        applyTransition(activity);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected void finishActivity(Activity activity){
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            activity.finishAfterTransition();
        } else {
            activity.finish();
        }
    }

    protected void dismissProgressDialog(){
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    protected void showServerFailureMessage(View view, String failureMessage){
        dismissProgressDialog();

        if (failureMessage == null || "".equals(failureMessage.trim()))
            failureMessage = getResources().getString(R.string.server_error);

        UtilityMethods.showSnackbar(failureMessage, view, this);

    }

}
