package joystick.byteis.com.joystick.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import java.nio.charset.Charset;
import java.util.Random;

import joystick.byteis.com.joystick.R;
import joystick.byteis.com.joystick.client.ClientCallback;
import joystick.byteis.com.joystick.data.SharedPref;
import joystick.byteis.com.joystick.databinding.ActivityChangePasswordBinding;
import joystick.byteis.com.joystick.model.ServerResponse;
import joystick.byteis.com.joystick.util.AESencrp;
import joystick.byteis.com.joystick.util.UtilityMethods;

/**
 * Created by Dina on 16/11/2015.
 */
public class ChangePasswordActivity extends BaseActivity implements View.OnClickListener, ClientCallback {

    private ActivityChangePasswordBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(ChangePasswordActivity.this, R.layout.activity_change_password);
        binding.setListener(this);
    }

    private void resetValidationError() {
        setError(binding.oldPasswordTextInput, null);
        setError(binding.newPasswordTextInput, null);
        setError(binding.repeatPasswordTextInput, null);
    }

    @Override
    public void onClick(View v) {
        resetValidationError();
        String oldPasswordText = binding.oldPasswordTextInput.getEditText().getText().toString();
        String newPasswordText = binding.newPasswordTextInput.getEditText().getText().toString();
        String newPasswordTextRepeated = binding.repeatPasswordTextInput.getEditText().getText().toString();

        if (isValidData(oldPasswordText, newPasswordText, newPasswordTextRepeated)) {
            showProgress();
            apiClient.changePassword(newPasswordText, newPasswordTextRepeated, this);
        }
    }

    private void showProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();

        progressDialog = showProgressDialog(ChangePasswordActivity.this.getResources().getString(R.string.please_Wait), false, true);
    }

    private boolean isValidData(String oldPassword, String newPassword, String repeatPassword) {
        boolean isValid = true;
        if (oldPassword == null || "".equals(oldPassword.trim()) || newPassword == null || "".equals(newPassword.trim()) || repeatPassword == null || "".equals(repeatPassword.trim())) {
            UtilityMethods.showSnackbar(getResources().getString(R.string.invalid_fields), binding.layoutChangePassword, this);
            isValid = false;

            if (oldPassword == null || "".equals(oldPassword.trim()))
                setError(binding.oldPasswordTextInput, getResources().getString(R.string.invalid_password));

            if (newPassword == null || "".equals(newPassword.trim()))
                setError(binding.newPasswordTextInput, getResources().getString(R.string.invalid_password));

            if (repeatPassword == null || "".equals(repeatPassword.trim()))
                setError(binding.repeatPasswordTextInput, getResources().getString(R.string.invalid_password));
        }

        byte[] encryptKey = SharedPref.LoadString(this, SharedPref.ENCRYPRION_KEY).getBytes(Charset.forName("UTF-8"));
        try {
            if (!oldPassword.equals(AESencrp.decrypt(SharedPref.LoadString(this, SharedPref.PASSWORD_KEY), encryptKey))) {
                setError(binding.oldPasswordTextInput, getResources().getString(R.string.incorrect_old_password));
                isValid = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isValid;
    }

    @Override
    public void onSuccess(Object o, String responseMessage) {
        String message = ((ServerResponse) o).getMessage();
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();

        UtilityMethods.showSnackbar(message, binding.layoutChangePassword, this);
        byte[] encryptKeyValue = UtilityMethods.createRandomKey(new Random());
        try {
            SharedPref.SaveString(ChangePasswordActivity.this, SharedPref.ENCRYPRION_KEY, new String(encryptKeyValue, "UTF-8"));
            SharedPref.SaveString(ChangePasswordActivity.this, SharedPref.PASSWORD_KEY, AESencrp.encrypt(binding.newPasswordEditText.getText().toString(), encryptKeyValue));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onFailure(String failureMessage) {
        showServerFailureMessage(binding.layoutChangePassword, failureMessage);
    }
}
