package joystick.byteis.com.joystick.client;

/**
 * Created by Dina on 10/11/2015.
 */
public interface ClientCallback<T> {

    public void onSuccess(T t, String responseMessage);

    public void onFailure(String failureMessage);

}
