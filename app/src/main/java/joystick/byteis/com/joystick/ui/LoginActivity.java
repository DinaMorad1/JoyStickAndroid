package joystick.byteis.com.joystick.ui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;

import com.facebook.GraphResponse;
import com.google.gson.internal.LinkedTreeMap;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Random;

import joystick.byteis.com.joystick.R;
import joystick.byteis.com.joystick.client.ApiClient;
import joystick.byteis.com.joystick.client.ClientCallback;
import joystick.byteis.com.joystick.data.SharedPref;
import joystick.byteis.com.joystick.databinding.ActivityLoginBinding;
import joystick.byteis.com.joystick.model.User;
import joystick.byteis.com.joystick.util.AESencrp;
import joystick.byteis.com.joystick.util.UtilityMethods;



/**
 * Created by Dina on 11/11/2015.
 */
public class LoginActivity extends BaseSocialActivity implements View.OnClickListener, ClientCallback {//, FacebookCallback<LoginResult>, GraphRequest.GraphJSONObjectCallback {

    private ActivityLoginBinding binding;
    private String password;

    private String consumerKey = null;
    private String consumerSecret = null;
    private String callbackUrl = null;
    private String oAuthVerifier = null;


    /* unique number for getting results from "TwitterLoginWebViewActivity" Activity */
    public static final int WEBVIEW_REQUEST_CODE = 101;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.setListener(LoginActivity.this);
        adjustTextStyle();
        // init Twitter Configurations to use it when login with twitter account.
      //  initTwitterConfigs();
        // init Facebook Configurations to use it when login with facebook account.

        initFacebookLogin();
        initTwitterLogin();

    }

   /* private void initTwitterConfigs() {
        consumerKey = getResources().getString(R.string.twitter_consumer_key);
        consumerSecret = getResources().getString(R.string.twitter_consumer_secret);
        callbackUrl = getResources().getString(R.string.twitter_callback);
        oAuthVerifier = getResources().getString(R.string.twitter_oauth_verifier);
    }*/

    private void initTwitterLogin(){
        binding.twitterLoginButton.setCallback(twitterCallback);
    }


    @Override
    protected void onTwitterSuccess(Result<TwitterSession> result) {
        super.onTwitterSuccess(result);
        showProgress();
        apiClient.socialLogin(twitterUserEmail, twitterUserEmail, twitterToken, ApiClient.SOCIAL_TWITTER_PROVIDER, twitterUserId, LoginActivity.this);

    }

    @Override
    protected void onTwitterFailure(TwitterException e) {
        super.onTwitterFailure(e);
        UtilityMethods.showSnackbar("Failed Twitter signin", binding.layoutActivityLogin, LoginActivity.this);

    }

    private void initFacebookLogin() {
        binding.fbLoginButton.setReadPermissions("email");
        binding.fbLoginButton.registerCallback(facebookCallbackManager, LoginActivity.this);
    }


    private void adjustTextStyle() {
        binding.forgotPassword.setPaintFlags(binding.forgotPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        binding.register.setPaintFlags(binding.register.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    public void onForgotPasswordClicked(View view) {
        Intent forgotPasswordIntent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
        startActivityWithTransition(LoginActivity.this, forgotPasswordIntent);
    }

    public void onSignupClicked(View view) {
        Intent registerIntent = new Intent(LoginActivity.this, SignupActivity.class);
        startActivityWithTransition(LoginActivity.this, registerIntent);
    }

    private void resetValidations() {
        setError(binding.emailTextInput, null);
        setError(binding.passwordTextInput, null);
    }

    private boolean isValidData(String email, String password) {
        boolean isValid = true;
        if (email == null || "".equals(email.trim()) || password == null || "".equals(password.trim())) {
            UtilityMethods.showSnackbar(getResources().getString(R.string.invalid_fields), binding.layoutActivityLogin, LoginActivity.this);
            isValid = false;

            if (email == null || "".equals(email.trim()) || !isEmailValid(email))
                setError(binding.emailTextInput, getResources().getString(R.string.invalid_email));
            else
                setError(binding.emailTextInput, null);

            if (password == null || "".equals(password.trim()))
                setError(binding.passwordTextInput, getResources().getString(R.string.invalid_password));
            else
                setError(binding.passwordTextInput, null);
        }
        return isValid;
    }

    @Override
    public void onClick(View v) {
            String email = binding.emailEditText.getText().toString();
            password = binding.passwordEditText.getText().toString();
            resetValidations();

            if (isValidData(email, password)) {
                showProgress();
                apiClient.userLogin(email, password, LoginActivity.this);
            }

       /* }else if (v.getId() == binding.buttonTwitterLogin.getId()) {
            showProgress();
            //loginWithTwitter();
        }*/

    }

    @Override
    public void onSuccess(Object o, String responseMessage) {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();

        byte[] encryptKeyValue = UtilityMethods.createRandomKey(new Random());
        try {
            SharedPref.SaveString(LoginActivity.this, SharedPref.ENCRYPRION_KEY, new String(encryptKeyValue, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try {
            SharedPref.SaveString(LoginActivity.this, SharedPref.PASSWORD_KEY, AESencrp.encrypt(password, encryptKeyValue));
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (o != null) {
            SharedPref.SaveString(LoginActivity.this, SharedPref.USER_TOKEN, ((User) o).getAuth_token());
//            ((User)o).setAvatar();
            LinkedTreeMap avatar = (LinkedTreeMap) ((User)o).getAvatar();
            String url = (String) ((LinkedTreeMap)((LinkedTreeMap)((LinkedTreeMap)avatar).get("url")).get("avatar")).get("url");
            ((User)o).setAvatarUrl(url);
            ((User) o).save();
        }
        if ((responseMessage == null || "".equals(responseMessage.trim())) && o != null)
            responseMessage = getResources().getString(R.string.login_success);

        UtilityMethods.showSnackbar(responseMessage, binding.layoutActivityLogin, LoginActivity.this);

        //ToDo: adjust the correct navigation sequence
        Intent settingsIntent = new Intent(LoginActivity.this, SettingsActivity.class);
        startActivityWithTransition(LoginActivity.this, settingsIntent);
        finishActivity(this);
    }

    @Override
    public void onFailure(String failureMessage) {
        showServerFailureMessage(binding.layoutActivityLogin, failureMessage);
    }

    @Override
    public void onCompleted(JSONObject object, GraphResponse response) {
        super.onCompleted(object, response);
        showProgress();
        apiClient.socialLogin(facebookUserEmail, facebookUserEmail, facebookToken, ApiClient.SOCIAL_FACEBOOK_PROVIDER, facebookUserId, LoginActivity.this);
    }


    private void showProgress() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();

        progressDialog = showProgressDialog(LoginActivity.this.getResources().getString(R.string.please_Wait), false, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        binding.twitterLoginButton.onActivityResult(requestCode, resultCode, data);
    }
}