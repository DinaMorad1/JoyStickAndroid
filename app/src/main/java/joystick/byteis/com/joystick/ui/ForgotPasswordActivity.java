package joystick.byteis.com.joystick.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import joystick.byteis.com.joystick.R;
import joystick.byteis.com.joystick.client.ClientCallback;
import joystick.byteis.com.joystick.databinding.ActivityForgotPasswordBinding;
import joystick.byteis.com.joystick.util.UtilityMethods;

/**
 * Created by Dina on 11/11/2015.
 */
public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener, ClientCallback {

    private ActivityForgotPasswordBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(ForgotPasswordActivity.this, R.layout.activity_forgot_password);
        binding.setListener(this);
    }

    @Override
    public void onClick(View v) {
        resetValidationError();
        String userEmail = binding.emailEditText.getText().toString();

        if (userEmail == null || "".equals(userEmail.trim()) || !isEmailValid(userEmail.trim())) {
            UtilityMethods.showSnackbar(getResources().getString(R.string.invalid_email), binding.layoutForgotPassword, ForgotPasswordActivity.this);
            setError(binding.emailTextInput, getResources().getString(R.string.invalid_email));
        } else {
            dismissProgressDialog();
            progressDialog = showProgressDialog(getResources().getString(R.string.please_Wait), false, true);
            apiClient.forgotPassword(binding.emailEditText.getText().toString(), ForgotPasswordActivity.this);
        }
    }

    private void resetValidationError(){
        setError(binding.emailTextInput, null);
    }

    @Override
    public void onSuccess(Object o, String responseMessage) {
        progressDialog.dismiss();
        if (responseMessage != null && !"".equals(responseMessage.trim()))
            UtilityMethods.showSnackbar(responseMessage, binding.layoutForgotPassword, ForgotPasswordActivity.this);
    }

    @Override
    public void onFailure(String failureMessage) {
        showServerFailureMessage(binding.layoutForgotPassword, failureMessage);
    }
}
