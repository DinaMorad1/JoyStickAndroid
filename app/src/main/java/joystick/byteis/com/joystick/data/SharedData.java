package joystick.byteis.com.joystick.data;

import android.app.Application;

import com.activeandroid.ActiveAndroid;
import com.facebook.FacebookSdk;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Dina on 10/11/2015.
 */
public class SharedData extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "euSAEiTGLOuYkygBcZgOrL4iQ";//"fhetzu2KIVIL9icXcg8x52AnM";
    private static final String TWITTER_SECRET = "gYuHSyxrucohEkVBpMrljh3Mijvfh0C4mysCDb8onUDDQXnY3D";//"XFgJGleIHBIXMhUCNpe8K7yyLVeA3xIkn4sDl8G0EOxBTPenVd";


    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        ActiveAndroid.initialize(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
    }
}
