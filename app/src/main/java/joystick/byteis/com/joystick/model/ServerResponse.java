package joystick.byteis.com.joystick.model;

/**
 * Created by Dina on 16/11/2015.
 */
public class ServerResponse {

    private boolean success;

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}
