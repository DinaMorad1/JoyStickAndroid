package joystick.byteis.com.joystick.ui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;

import com.facebook.AccessToken;

import joystick.byteis.com.joystick.R;
import joystick.byteis.com.joystick.data.SharedPref;

/**
 * Created by Dina on 11/11/2015.
 */
public class SplashActivity extends BaseActivity {

    private final int SPLASH_DISPLAY_LENGTH = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(SplashActivity.this, R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //check user login state

                if (SharedPref.checkKey(SplashActivity.this, SharedPref.USER_TOKEN) && SharedPref.LoadString(SplashActivity.this, SharedPref.USER_TOKEN) != null && !"".equals(SharedPref.LoadString(SplashActivity.this, SharedPref.USER_TOKEN).trim())) {
                    if (AccessToken.getCurrentAccessToken() != null && !AccessToken.getCurrentAccessToken().isExpired())
                        navigateToSettings();
                    else
                        navigateToLogin();
                } else
                    navigateToLogin();
            }
        }, SPLASH_DISPLAY_LENGTH);


    }

    private void navigateToLogin() {
        Intent loginIntent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivityWithTransition(SplashActivity.this, loginIntent);
        finishActivity(SplashActivity.this);
    }

    private void navigateToSettings() {
        Intent settingsIntent = new Intent(SplashActivity.this, SettingsActivity.class);
        startActivityWithTransition(SplashActivity.this, settingsIntent);
        finishActivity(SplashActivity.this);
    }
}
