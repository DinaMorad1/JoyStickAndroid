package joystick.byteis.com.joystick.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.google.gson.internal.LinkedTreeMap;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;

import joystick.byteis.com.joystick.R;
import joystick.byteis.com.joystick.client.ApiClient;
import joystick.byteis.com.joystick.client.ClientCallback;
import joystick.byteis.com.joystick.databinding.ActivitySignupBinding;
import joystick.byteis.com.joystick.model.User;
import joystick.byteis.com.joystick.util.UtilityMethods;

/**
 * Created by Dina on 11/11/2015.
 */
public class SignupActivity extends BaseSocialActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener, ClientCallback {

    private ActivitySignupBinding binding;
    private DatePickerDialog datePickerDialog;
    public static final String DATEPICKER_TAG = "datepicker";
    private Calendar birthDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(SignupActivity.this, R.layout.activity_signup);
        binding.setListener(this);
        birthDate = Calendar.getInstance();
        initFacebookLogin();
        initTwitterSignup();
    }

    private void initTwitterSignup(){
        binding.twitterSignupButton.setCallback(twitterCallback);
    }
    private void initFacebookLogin() {
        //ToDo: check if user has privacy settings
        //ToDo: check accessing user birthday
        binding.fbLoginButton.setReadPermissions(Arrays.asList("public_profile","email"));//,"user_birthday"));
//        binding.fbLoginButton.setReadPermissions(Arrays.asList("public_profile, email, user_birthday"));
        binding.fbLoginButton.registerCallback(facebookCallbackManager, SignupActivity.this);
    }

    private void refreshValidationErrors() {
        setError(binding.firstNameTextInput, null);
        setError(binding.lastNameTextInput, null);
        setError(binding.emailTextInput, null);
        setError(binding.passwordTextInput, null);
        setError(binding.passwordRepeatTextInput, null);
    }

    @Override
    public void onClick(View v) {
        refreshValidationErrors();
        String firstName = binding.firstNameEditText.getText().toString();
        String lastName = binding.lastNameEditText.getText().toString();
        String password = binding.passwordEditText.getText().toString();
        String email = binding.emailEditText.getText().toString();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String formattedBirthDate = formatter.format(birthDate.getTime());

        if (isValidData(firstName, lastName, email, password)) {
            showProgress();
            apiClient.userSignUp(email, password, firstName, lastName, formattedBirthDate, SignupActivity.this);
        }
    }

    private void showProgress(){
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();

        progressDialog = showProgressDialog(getResources().getString(R.string.please_Wait), false, true);
    }

    private boolean isValidData(String firstName, String lastName, String email, String password) {
        boolean isValid = true;
        if (firstName == null || "".equals(firstName.trim()) || lastName == null || "".equals(lastName.trim()) || password == null || "".equals(password.trim()) || email == null || "".equals(email.trim())) {
            UtilityMethods.showSnackbar(getResources().getString(R.string.invalid_fields), binding.layoutActivitySignup, SignupActivity.this);
            isValid = false;

            if (firstName == null || "".equals(firstName.trim()))
                setError(binding.firstNameTextInput, getResources().getString(R.string.invalid_first_name));

            if (lastName == null || "".equals(lastName.trim()))
                setError(binding.lastNameTextInput, getResources().getString(R.string.invalid_last_name));

            if (email == null || "".equals(email.trim()) || !isEmailValid(email.trim()))
                setError(binding.emailTextInput, getResources().getString(R.string.invalid_email));

            if (password == null || "".equals(password.trim()))
                setError(binding.passwordTextInput, getResources().getString(R.string.invalid_password));
        } else if (!binding.passwordEditText.getText().toString().equals(binding.activitySignupRepeatPasswordEditText.getText().toString())) {
            UtilityMethods.showSnackbar(getResources().getString(R.string.invalid_repeat_password), binding.layoutActivitySignup, SignupActivity.this);
            isValid = false;
            setError(binding.passwordTextInput, getResources().getString(R.string.invalid_repeat_password));
            setError(binding.passwordRepeatTextInput, getResources().getString(R.string.invalid_repeat_password));
        }
        return isValid;
    }

    public void onSelectDateClicked(View view) {
        //ToDo: set month restriction
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = DatePickerDialog.newInstance(SignupActivity.this, year, month, day);
        datePickerDialog.setYearRange(1919, year);
        datePickerDialog.setFirstDayOfWeek(Calendar.DAY_OF_WEEK);
        datePickerDialog.show(getFragmentManager(), DATEPICKER_TAG);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        birthDate = Calendar.getInstance();
        birthDate.set(Calendar.YEAR, year);
        birthDate.set(Calendar.MONTH, monthOfYear);
        birthDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String formattedBirthDate = formatter.format(birthDate.getTime());
        binding.dateTextView.setText(formattedBirthDate);
    }

    @Override
    public void onSuccess(Object o, String responseMessage) {
        if (progressDialog != null)
            progressDialog.dismiss();
        
        if (responseMessage != null && !"".equals(responseMessage.trim()))
            UtilityMethods.showSnackbar(responseMessage, binding.layoutActivitySignup, SignupActivity.this);
        else if (o != null)
            UtilityMethods.showSnackbar(getResources().getString(R.string.user_successfully_created), binding.layoutActivitySignup, SignupActivity.this);

        if (o != null) {
            LinkedTreeMap avatar = (LinkedTreeMap) ((User)o).getAvatar();
            String url = (String) ((LinkedTreeMap)((LinkedTreeMap)((LinkedTreeMap)avatar).get("url")).get("avatar")).get("url");
            ((User)o).setAvatarUrl(url);
            ((User) o).save();
        }

        Intent settingsIntent = new Intent(this, SettingsActivity.class);
        startActivityWithTransition(this, settingsIntent);
        finishActivity(this);
    }

    @Override
    public void onFailure(String failureMessage) {
        showServerFailureMessage(binding.layoutActivitySignup, failureMessage);
    }

    @Override
    public void onCompleted(JSONObject object, GraphResponse response) {
        super.onCompleted(object, response);
        showProgress();
        Profile profile = Profile.getCurrentProfile();
        String firstName = profile.getFirstName();
        String lastName = profile.getLastName();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String formattedBirthDate = formatter.format(birthDate.getTime());
        apiClient.socialSignup(facebookUserEmail, facebookUserEmail, facebookToken, firstName, lastName, ApiClient.SOCIAL_FACEBOOK_PROVIDER,facebookUserId, formattedBirthDate, SignupActivity.this);
    }

    @Override
    protected void onTwitterSuccess(Result<TwitterSession> result) {
        super.onTwitterSuccess(result);
        showProgress();
        String [] fullName = twitterUserName.split("\\s+");
        String firstName ;
        String lastName ;
        if(fullName.length >1) {
            firstName = fullName[0];
            lastName = fullName[1];
        }else {
            firstName = fullName[0];
            lastName = "invaild";
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String formattedBirthDate = formatter.format(birthDate.getTime());
        apiClient.socialSignup(twitterUserEmail, twitterUserEmail, twitterToken, firstName, lastName, ApiClient.SOCIAL_TWITTER_PROVIDER, twitterUserId, formattedBirthDate, SignupActivity.this);

    }

    @Override
    protected void onTwitterFailure(TwitterException e) {
        super.onTwitterFailure(e);
        UtilityMethods.showSnackbar("Failed Twitter signup",binding.layoutActivitySignup,SignupActivity.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        binding.twitterSignupButton.onActivityResult(requestCode, resultCode, data);
    }
}
