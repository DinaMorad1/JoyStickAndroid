package joystick.byteis.com.joystick.client;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Base64;

import com.android.volley.Request;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

import joystick.byteis.com.joystick.R;
import joystick.byteis.com.joystick.data.SharedPref;
import joystick.byteis.com.joystick.model.ServerResponse;
import joystick.byteis.com.joystick.model.User;

/**
 * Created by Dina on 10/11/2015.
 */
public class ApiClientImp extends VolleyHelper implements ApiClient {

    private static ApiClientImp apiClient;
    private String baseUrl;

    private ApiClientImp(Context context) {
        super(context);
        baseUrl = context.getResources().getString(R.string.base_url);
    }

    public static ApiClientImp getInstance(Context context) {
        if (apiClient == null)
            apiClient = new ApiClientImp(context);
        return apiClient;
    }


    @Override
    public void userLogin(String userEmail, String password, ClientCallback clientCallback) {
        JSONObject params = new JSONObject();
        JSONObject user = new JSONObject();
        try {
            params.put(ApiClient.USER_EMAIL, userEmail);
            params.put(ApiClient.USER_PASSWORD, password);
            user.put(ApiClient.USER, params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        scheduleJsonObjectRequest(Request.Method.POST, baseUrl + ApiClient.USER_SIGNIN_URL, user, clientCallback, User.class);
    }

    @Override
    public void forgotPassword(String userEmail, ClientCallback callback) {
        JSONObject params = new JSONObject();

        try {
            params.put(ApiClient.USER_EMAIL, userEmail);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        scheduleJsonObjectRequest(Request.Method.GET, baseUrl + ApiClient.FORGOT_PASSWORD_URL + "?" + ApiClient.USER_EMAIL + "=" + userEmail, null, callback, null);
    }

    public void userSignUp(String userEmail, String password, String firstName, String lastName, String formattedBirthDate, ClientCallback clientCallback) {
        JSONObject params = new JSONObject();
        JSONObject user = new JSONObject();
        try {
            params.put(ApiClient.USER_EMAIL, userEmail);
            params.put(ApiClient.USER_PASSWORD, password);
            params.put(ApiClient.FIRST_NAME, firstName);
            params.put(ApiClient.LAST_NAME, lastName);
            params.put(ApiClient.BIRTH_DATE, formattedBirthDate);
            user.put(ApiClient.USER, params);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        scheduleJsonObjectRequest(Request.Method.POST, baseUrl + ApiClient.USERS_URL, user, clientCallback, User.class);
    }

    @Override
    public void changeEmail(String newEmail, ClientCallback callback) {
        JSONObject params = new JSONObject();
        JSONObject sendObj = new JSONObject();
        String token = SharedPref.LoadString(context, SharedPref.USER_TOKEN);
        String birthDate = User.getCurrentUser().getBirth_date();

        try {
            params.put(ApiClient.USER_EMAIL, newEmail);
            params.put(ApiClient.BIRTH_DATE, birthDate);
            sendObj.put(ApiClient.USER, params);
            sendObj.put(ApiClient.TOKEN, token);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        scheduleJsonObjectRequest(Request.Method.PUT, baseUrl + ApiClient.USERS_URL, sendObj, callback, User.class);
    }

    @Override
    public void socialLogin(String email, String password, String socialToken, String provider, String userId, ClientCallback clientCallback) {
        JSONObject userJson = new JSONObject();
        JSONObject userParams = new JSONObject();

        try {
            userParams.put(ApiClient.USER_EMAIL, email);
            userParams.put(ApiClient.USER_PASSWORD, password);
            userParams.put(ApiClient.SOCIAL_TOKEN, socialToken);
            userParams.put(ApiClient.SOCIAL_PROVIDER, provider);
            userParams.put(ApiClient.SOCIAL_USER_ID, userId);
            userJson.put(ApiClient.USER, userParams);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        scheduleJsonObjectRequest(Request.Method.POST, baseUrl + ApiClient.USER_SIGNIN_URL, userJson, clientCallback, User.class);
    }

    @Override
    public void socialSignup(String email, String password, String socialToken, String firstName, String lastName, String provider, String userId, String birthDate, ClientCallback clientCallback) {
        JSONObject userJson = new JSONObject();
        JSONObject userParams = new JSONObject();

        try {
            userParams.put(ApiClient.USER_EMAIL, email);
            userParams.put(ApiClient.USER_PASSWORD, password);
            userParams.put(ApiClient.FIRST_NAME, firstName);
            userParams.put(ApiClient.LAST_NAME, lastName);
            userParams.put(ApiClient.SOCIAL_PROVIDER, provider);
            userParams.put(ApiClient.SOCIAL_TOKEN, socialToken);//ToDo: social token can't be given in case of registration!
            userParams.put(ApiClient.BIRTH_DATE, birthDate);
            userParams.put(ApiClient.SOCIAL_USER_ID, userId);
            userJson.put(ApiClient.USER, userParams);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        scheduleJsonObjectRequest(Request.Method.POST, baseUrl + ApiClient.USERS_URL, userJson, clientCallback, User.class);
    }

    @Override
    public void changePassword(String newPassword, String confirmedNewPassword, ClientCallback clientCallback) {
        JSONObject params = new JSONObject();
        JSONObject userParams = new JSONObject();
        String token = SharedPref.LoadString(context, SharedPref.USER_TOKEN);


        try {
            userParams.put(ApiClient.USER_PASSWORD, newPassword);
            userParams.put(ApiClient.USER_CONFIRMED_PASSWORD, confirmedNewPassword);
            params.put(ApiClient.USER, userParams);
            params.put(ApiClient.TOKEN, token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        scheduleJsonObjectRequest(Request.Method.PUT, baseUrl + ApiClient.NEW_PASSWORD_URL, params, clientCallback, ServerResponse.class);
    }

    @Override
    public void updateUserProfile(Bitmap bitmap, String firstName, String email, ClientCallback callback) {
        User currentUser = User.getCurrentUser();
        String encodedImage = getEncodedImage(bitmap);
        if (encodedImage == null)
            encodedImage = currentUser.getAvatarUrl();
        JSONObject userParams = new JSONObject();
        JSONObject params = new JSONObject();
        String birthDate = currentUser.getBirth_date();
        String token = SharedPref.LoadString(context, SharedPref.USER_TOKEN);
        try {
            if (encodedImage != null) {
                userParams.put(ApiClient.USER_AVATAR_CONTENT_TYPE, "image/jpeg");
                userParams.put(ApiClient.USER_AVATAR_DATA, encodedImage);
            }
            userParams.put(ApiClient.BIRTH_DATE, birthDate);
            userParams.put(ApiClient.FIRST_NAME, firstName);
            userParams.put(ApiClient.USER_EMAIL, email);
            params.put(ApiClient.USER, userParams);
            params.put(ApiClient.TOKEN, token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        scheduleJsonObjectRequest(Request.Method.PUT, baseUrl + ApiClient.USERS_URL, params, callback, User.class);

    }

    private String getEncodedImage(Bitmap bitmap) {
        String encodedImage = null;
        if (bitmap != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos);
            byte[] b = baos.toByteArray();
            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        }
        return encodedImage;
    }

    @Override
    public void updateAvatar(Bitmap bitmap, ClientCallback callback) {
        JSONObject params = new JSONObject();
        JSONObject userParams = new JSONObject();
        String encodedImage = getEncodedImage(bitmap);
        String birthDate = User.getCurrentUser().getBirth_date();
        String token = SharedPref.LoadString(context, SharedPref.USER_TOKEN);

        try {
            userParams.put(ApiClient.USER_AVATAR_CONTENT_TYPE, "image/jpeg");
            userParams.put(ApiClient.USER_AVATAR_DATA, encodedImage);
            userParams.put(ApiClient.BIRTH_DATE, birthDate);
            params.put(ApiClient.USER, userParams);
            params.put(ApiClient.TOKEN, token);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        scheduleJsonObjectRequest(Request.Method.PUT, baseUrl + ApiClient.USERS_URL, params, callback, User.class);
    }
}
