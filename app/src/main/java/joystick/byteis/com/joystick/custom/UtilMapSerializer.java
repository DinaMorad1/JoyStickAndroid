package joystick.byteis.com.joystick.custom;

import com.activeandroid.serializer.TypeSerializer;
import com.google.gson.internal.LinkedTreeMap;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by Dina on 17/11/2015.
 */
public class UtilMapSerializer extends TypeSerializer {
    @Override
    public Class<?> getDeserializedType() {
        return LinkedTreeMap.class;
    }

    @Override
    public Class<?> getSerializedType() {
        return String.class;
    }

    @Override
    public String serialize(Object data) {
        if (data == null) {
            return null;
        }

        // Transform a Map<String, Object> to JSON and then to String
        return new JSONObject((LinkedTreeMap<String, Object>) data).toString();
    }

    @Override
    public LinkedTreeMap<String, Object> deserialize(Object data) {
        if (data == null) {
            return null;
        }

        // Properties of Model
        LinkedTreeMap<String, Object> map = new LinkedTreeMap<String, Object>();

        try {
            JSONObject json = new JSONObject((String) data);

            for(Iterator it = json.keys(); it.hasNext();) {
                String key = (String) it.next();

                map.put(key, json.get(key));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return map;
    }
}