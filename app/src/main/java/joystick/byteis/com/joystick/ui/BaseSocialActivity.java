package joystick.byteis.com.joystick.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;
import org.json.JSONException;
import org.json.JSONObject;
import io.fabric.sdk.android.Fabric;
import java.util.Arrays;
import joystick.byteis.com.joystick.data.SharedPref;
import joystick.byteis.com.joystick.util.UtilityMethods;

/**
 * Created by Dina on 15/11/2015.
 */
public class BaseSocialActivity extends BaseActivity implements FacebookCallback<LoginResult>, GraphRequest.GraphJSONObjectCallback {


    protected CallbackManager facebookCallbackManager;
    protected String facebookToken;
    protected String facebookUserEmail;
    protected String facebookUserId;
    protected String gender;
    protected LoginManager loginManager;

    // Twitter data
    protected String twitterToken;
    protected String twitterUserName;
    protected String twitterUserId;
    protected String twitterUserEmail = "nooneknow2050@gmail.com";
    protected String provid ;

    //protected Callback<TwitterSession> twitterCallbackManager;
    //protected String provider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginManager = LoginManager.getInstance();
        facebookCallbackManager = CallbackManager.Factory.create();

        if (SharedPref.checkKey(this, SharedPref.USER_TOKEN) && SharedPref.LoadString(this, SharedPref.USER_TOKEN) != null && !"".equals(SharedPref.LoadString(this, SharedPref.USER_TOKEN).trim())) {
            loginManager.logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends"));//email
        }
    }


    protected Callback<TwitterSession> twitterCallback = new Callback<TwitterSession>() {
        @Override
        public void success(Result<TwitterSession> result) {
          onTwitterSuccess(result);
        }

        @Override
        public void failure(TwitterException e) {
            onTwitterFailure(e);
        }
    };

    protected void onTwitterSuccess(Result<TwitterSession> result){
        TwitterSession session = result.data;
        twitterToken = session.getAuthToken().token;
        Log.d("twitterAccessToken", twitterToken);
        Log.d("twitterTokenisExpired",""+session.getAuthToken().isExpired());
        Log.d("twitterTokensecret",""+session.getAuthToken().secret);
        twitterUserName = session.getUserName();
        twitterUserId = String.valueOf(session.getUserId());
        // new callback for getting twitter email
      /*  TwitterAuthClient authClient = new TwitterAuthClient();
        authClient.requestEmail(session,twitterEmailCallback);*/

    }

    protected void onTwitterFailure(TwitterException e){
        e.printStackTrace();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        facebookCallbackManager.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        facebookToken = loginResult.getAccessToken().getToken();
        AccessToken.setCurrentAccessToken(loginResult.getAccessToken());
        SharedPref.SaveString(BaseSocialActivity.this, SharedPref.FACEBOOK_TOKEN, facebookToken);
        scheduleDataRequest();
    }

    private void scheduleDataRequest() {
        GraphRequest graphRequest = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), BaseSocialActivity.this);
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture,birthday,gender");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onError(FacebookException error) {

    }

    //graphhuser callback
    @Override
    public void onCompleted(JSONObject object, GraphResponse response) {
        JSONObject json = response.getJSONObject();
        try {
            if (json != null) {
                if (json.has("email"))
                    facebookUserEmail = json.getString("email");

                if (json.has("id"))
                    facebookUserId = json.getString("id");

                if (json.has("gender"))
                    gender = json.getString("gender");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
