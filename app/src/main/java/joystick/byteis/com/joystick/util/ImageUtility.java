package joystick.byteis.com.joystick.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Environment;

import java.io.File;

/**
 * Created by Dina on 16/11/2015.
 */
public class ImageUtility {
    public static Uri outputFileUri;
    private static int widthImage = 350, heightImage = 350;

    public static void createImage(Context context) {
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "MyDir" + File.separator);
        root.mkdirs();

        final String fname = System.currentTimeMillis() + ".jpg";
        final File sdImageMainDirectory = new File(root, fname);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);
        sdImageMainDirectory.deleteOnExit();
    }

    public static Bitmap getRoundedBitmap(Bitmap bitmap) {

        Bitmap resized = Bitmap.createScaledBitmap(bitmap, widthImage, heightImage, true);
        Bitmap result = Bitmap.createBitmap(widthImage, heightImage, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(result);
        Paint paint = new Paint();

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawCircle(widthImage / 2, heightImage / 2, widthImage / 2, paint);

        // Mode.SRC_IN: Only the part of the source image that overlaps the destination image will be drawn.
        // REF: http://www.ibm.com/developerworks/java/library/j-mer0918/
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        Rect rect = new Rect(0, 0, widthImage, heightImage);
        canvas.drawBitmap(resized, rect, rect, paint);

        return result;
    }

}
