package joystick.byteis.com.joystick.ui;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import com.androidquery.AQuery;
import com.google.gson.internal.LinkedTreeMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import joystick.byteis.com.joystick.R;
import joystick.byteis.com.joystick.client.ApiClient;
import joystick.byteis.com.joystick.client.ApiClientImp;
import joystick.byteis.com.joystick.client.ClientCallback;
import joystick.byteis.com.joystick.databinding.ActivityEditProfileBinding;
import joystick.byteis.com.joystick.model.User;
import joystick.byteis.com.joystick.util.ImageUtility;
import joystick.byteis.com.joystick.util.UtilityMethods;

/**
 * Created by Dina on 16/11/2015.
 */
public class EditProfileActivity extends BaseActivity implements View.OnClickListener, ClientCallback {

    private ActivityEditProfileBinding binding;
    private AQuery aQuery;
    private Bitmap profileBitmap;
    private User loginUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile);
        loginUser = User.getCurrentUser();
        binding.setUser(loginUser);
        aQuery = new AQuery(this);
        binding.setListener(this);
        profileBitmap = null;
        showUserAvatar();
    }

    private void showUserAvatar() {
        User loginUser = User.getCurrentUser();
        updateUserAvatar(loginUser.getAvatarUrl());
    }

    private void updateUserAvatar(String avatarUrl) {
        aQuery.id(binding.userAvatar).image(avatarUrl);
    }


    public void onSelectImgClicked(View view) {
        startCameraIntent();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals("inline data");
                    }
                }

                Uri selectedImageUri;

                if (isCamera) {
                    selectedImageUri = ImageUtility.outputFileUri;
                } else {
                    selectedImageUri = data == null ? null : data.getData();
                }
                if (selectedImageUri == null) {
                    UtilityMethods.showSnackbar(getResources().getString(R.string.avatar_error), binding.layoutEditProfile, this);
                } else {

                    String selectedImagePath = getPathFromUri(selectedImageUri);
                    if (selectedImagePath != null) {
                        File file = new File(getPathFromUri(selectedImageUri));
                        Bitmap sourceBitmap = decodeFile(file);
                        try {
                            ExifInterface exif = new ExifInterface(getPathFromUri(selectedImageUri));
                            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                                Matrix matrix = new Matrix();
                                matrix.postRotate(90);
                                Bitmap rotatedBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight(), matrix, true);
//                            userAvatarImgView.setImageBitmap(ImageUtility.getScaledBitmap(rotatedBitmap, getScreenWidth(), (int) SettingsActivity.this.getResources().getDimension(R.dimen.collapsible_toolbar_height), getScreenHeight()));
//                                updateImage(rotatedBitmap);
                                profileBitmap = rotatedBitmap;
                            } else {
//                            userAvatarImgView.setImageBitmap(ImageUtility.getScaledBitmap(sourceBitmap, getScreenWidth(), (int) SettingsActivity.this.getResources().getDimension(R.dimen.collapsible_toolbar_height), getScreenHeight()));
//                                updateImage(sourceBitmap);
                                profileBitmap = sourceBitmap;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    private void updateImage(Bitmap bm) {
//        showProgress();
//        profileBitmap = bm;
//        apiClient.updateAvatar(bm, this);
    }

    private void showProgress() {
        dismissProgressDialog();
        progressDialog = showProgressDialog(this.getResources().getString(R.string.please_Wait), false, true);
    }

    private Bitmap decodeFile(File f) {
        Log.d("file decodig ", "..." + f.getAbsolutePath());
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            final int REQUIRED_SIZE = 560;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
        }
        return null;
    }


    public String getPathFromUri(Uri selectedImage) {

        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        String picturePath = "";
        if (cursor != null) {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
        } else {
            picturePath = selectedImage.getPath();
        }
        Log.d("imagePath", picturePath);
        return picturePath;
    }


    private void startCameraIntent() {
        ImageUtility.createImage(this);

        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, ImageUtility.outputFileUri);
            cameraIntents.add(intent);
        }

        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));
        int YOUR_SELECT_PICTURE_REQUEST_CODE = 1;
        startActivityForResult(chooserIntent, YOUR_SELECT_PICTURE_REQUEST_CODE);
    }

    @Override
    public void onClick(View v) {
        String firstName = binding.userNameEditText.getText().toString();
        String email = binding.userEmailEditText.getText().toString();
        if (isValidData(firstName, email)) {
            showProgress();
            apiClient.updateUserProfile(profileBitmap, firstName, email, this);
        }
    }

    private boolean isValidData(String firstName, String email) {
        boolean isValid = true;
        if (firstName == null || "".equals(firstName.trim()) || email == null || "".equals(email.trim())) {
            UtilityMethods.showSnackbar(getResources().getString(R.string.invalid_fields), binding.layoutEditProfile, this);
            isValid = false;

            if (email == null || "".equals(email.trim()) || !isEmailValid(email))
                setError(binding.userEmailTextInput, getResources().getString(R.string.invalid_email));
            else
                setError(binding.userEmailTextInput, null);

            if (firstName == null || "".equals(firstName.trim()))
                setError(binding.userNameTextInput, getResources().getString(R.string.invalid_password));
            else
                setError(binding.userNameTextInput, null);
        }
        return isValid;
    }


    @Override
    public void onSuccess(Object o, String responseMessage) {
        if (o != null) {
            User.removeLoginUser();
            User resultUser = (User) o;
            LinkedTreeMap avatar = (LinkedTreeMap) resultUser.getAvatar();
            String url = (String) ((LinkedTreeMap) ((LinkedTreeMap) ((LinkedTreeMap) avatar).get("url")).get("avatar")).get("url");
            ((User) o).setAvatarUrl(url);
            ((User) o).save();
            dismissProgressDialog();
            finishActivity(this);
        }
    }

    @Override
    public void onFailure(String failureMessage) {
        showServerFailureMessage(binding.layoutEditProfile, failureMessage);
    }
}
