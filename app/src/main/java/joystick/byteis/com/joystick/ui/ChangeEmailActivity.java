package joystick.byteis.com.joystick.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import joystick.byteis.com.joystick.R;
import joystick.byteis.com.joystick.client.ClientCallback;
import joystick.byteis.com.joystick.databinding.ActivityChangeEmailBinding;
import joystick.byteis.com.joystick.model.User;
import joystick.byteis.com.joystick.util.UtilityMethods;

/**
 * Created by Dina on 12/11/2015.
 */
public class ChangeEmailActivity extends BaseActivity implements View.OnClickListener, ClientCallback {

    private ActivityChangeEmailBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(ChangeEmailActivity.this, R.layout.activity_change_email);
        binding.setListener(this);
    }

    private void resetValidationError() {
        setError(binding.emailTextInput, null);
    }

    @Override
    public void onClick(View v) {
        resetValidationError();
        String newEmail = binding.emailEditText.getText().toString();
        if (newEmail == null || "".equals(newEmail.trim()) || !isEmailValid(newEmail.trim())) {
            UtilityMethods.showSnackbar(getResources().getString(R.string.invalid_email), binding.layoutChangeEmail, ChangeEmailActivity.this);
            setError(binding.emailTextInput, getResources().getString(R.string.invalid_email));
        } else {
            dismissProgressDialog();
            progressDialog = showProgressDialog(getResources().getString(R.string.please_Wait), false, true);
            apiClient.changeEmail(newEmail, ChangeEmailActivity.this);
        }
    }

    @Override
    public void onSuccess(Object o, String responseMessage) {
        dismissProgressDialog();

        if (o != null && (responseMessage == null || "".equals(responseMessage.trim())))
            UtilityMethods.showSnackbar(getResources().getString(R.string.email_successfully_changed) + ((User)o).getEmail(), binding.layoutChangeEmail, ChangeEmailActivity.this);

    }

    @Override
    public void onFailure(String failureMessage) {
        showServerFailureMessage(binding.layoutChangeEmail, failureMessage);
    }
}
